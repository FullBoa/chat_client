from app.app import create_app


app = create_app('client.conf')

if __name__ == '__main__':
    app.run()
