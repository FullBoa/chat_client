import requests
from flask import current_app


class UserService(object):
    """Сервис для аутентификации и регистрации пользователей"""

    def __init__(self):
        server_host = current_app.config['SERVER_HOST']
        server_port = current_app.config['SERVER_PORT']
        auth_route = current_app.config['SERVER_AUTH_ROUTE']
        register_route = current_app.config['SERVER_REGISTER_ROUTE']
        self.auth_url = 'http://{host}:{port}{auth_route}'.format(
            host=server_host,
            port=server_port,
            auth_route=auth_route
        )
        self.registration_url = 'http://{host}:{port}{register_route}'.format(
            host=server_host,
            port=server_port,
            register_route=register_route
        )

    def authorize(self, nick, password):
        """Аутентифицировать пользователя"""

        params = {'nick': nick, 'pwd': password}
        result = requests.post(self.auth_url, params=params)
        return result.status_code == requests.codes.ok

    def register(self, nick, password):
        """Зарегистрировать нового пользователя"""

        params = {'nick': nick, 'pwd': password}
        result = requests.post(self.registration_url, params=params)
        return result.status_code == requests.codes.ok
