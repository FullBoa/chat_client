import requests
from flask import current_app


class MessagesService(object):
    """Сервис для отправки и приёма сообщения"""

    def __init__(self):
        server_host = current_app.config['SERVER_HOST']
        server_port = current_app.config['SERVER_PORT']
        send_route = current_app.config['SERVER_SEND_ROUTE']
        receive_route = current_app.config['SERVER_RECEIVE_ROUTE']
        self.send_url = 'http://{host}:{port}{send_route}'.format(
            host=server_host,
            port=server_port,
            send_route=send_route
        )
        self.receive_url = 'http://{host}:{port}{receive_route}'.format(
            host=server_host,
            port=server_port,
            receive_route=receive_route
        )

    def send(self, nick, message):
        """Отправить сообщение"""

        params = {'nick': nick, 'message': message}
        result = requests.post(self.send_url, params=params)
        return result.status_code == requests.codes.ok

    def receive(self, nick):
        """Принять новые сообщения"""

        params = {'nick': nick}
        result = requests.post(self.receive_url, params=params)
        if result.status_code == requests.codes.ok:
            return result.json()
        else:
            return None
