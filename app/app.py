from flask import Flask
from app.user_manager import login_manager


def create_app(config_filename):

    app = Flask(__name__)
    app.config.from_pyfile(config_filename)
    login_manager.init_app(app)
    login_manager.session_protection = "strong"

    from app.controllers.chat_controller import chat_controller
    from app.controllers.login_controller import login_controller
    from app.controllers.registration_controller import registration_controller

    app.register_blueprint(chat_controller)
    app.register_blueprint(login_controller)
    app.register_blueprint(registration_controller)

    login_manager.login_view = '/login'
    return app
