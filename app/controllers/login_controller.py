from datetime import timedelta
from flask import Blueprint
from flask import current_app
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from flask_login import login_user, logout_user, login_required, current_user
from app.user import User
from app.services.user_service import UserService

login_controller = Blueprint('login_controller', __name__)


@login_controller.route('/login', methods=['GET', ])
def index():

    if current_user.is_authenticated:
        return redirect(url_for('chat_controller.index'))

    return render_template('login.html')


@login_controller.route('/login', methods=['POST'], )
def login():

    nick = request.form['nick']
    password = request.form['password']

    service = UserService()
    is_authorized = service.authorize(nick, password)
    if is_authorized:
        session.permanent = True
        lifetime = current_app.config['SESSION_LIFETIME']
        current_app.permanent_session_lifetime = timedelta(minutes=lifetime)
        login_user(User(nick), remember=True)
        session['user'] = nick
        session.new = True
        return redirect(url_for('chat_controller.index'))
    else:
        return render_template('login.html', error=True)


@login_controller.route('/login', methods=['DELETE', ])
@login_required
def logout():
    print(current_user.nick)
    session.clear()
    logout_user()

    return index()
