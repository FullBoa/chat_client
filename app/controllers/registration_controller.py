from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_login import current_user
from app.services.user_service import UserService

registration_controller = Blueprint('registration_controller', __name__)


@registration_controller.route('/register', methods=['GET', ])
def index():

    if current_user.is_authenticated:
        return redirect(url_for('chat_controller.index'))

    return render_template('registration.html')


@registration_controller.route('/register', methods=['POST', ])
def register():
    nick = request.form['nick']
    password = request.form['password']

    service = UserService()
    result = service.register(nick, password)
    if result:
        return redirect(url_for('login_controller.index'))
    else:
        return render_template('registration.html', error=True)
