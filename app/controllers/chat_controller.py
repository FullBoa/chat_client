from flask import Blueprint
from flask import jsonify
from flask import render_template
from flask import request
from flask_login import login_required, current_user
from werkzeug.exceptions import ServiceUnavailable
from app.services.messages_service import MessagesService

chat_controller = Blueprint('chat_controller', __name__)


@chat_controller.route('/', methods=['GET', ])
@login_required
def index():
    nick = current_user.nick
    service = MessagesService()
    result = service.receive(nick)
    if result is None:
        result = []
    return render_template('index.html', messages=result)


@chat_controller.route('/send', methods=['POST', ])
@login_required
def send():
    nick = current_user.nick
    data = request.get_json()
    message = data['message']
    service = MessagesService()
    if service.send(nick, message):
        return '', 200
    else:
        return ServiceUnavailable()


# Запрос не является идеомпотентным, поэтому используем метод POST
@chat_controller.route('/receive', methods=['POST', ])
@login_required
def receive():
    nick = current_user.nick
    service = MessagesService()
    result = service.receive(nick)
    return jsonify(result)
